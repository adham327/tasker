from datetime import timedelta, datetime
import pickle as p
import os
from main import dir_of_tsk



def load(loaded_dict):
    from global_config import (zero_time, _today, _month, _year, work_time,
                               rest_time, big_rest_time, rest_multplier,
                               tasks_before_15_minutes_rest, time_multplier)
    loaded_dict = dir_of_tsk + '/' + loaded_dict
    with open(loaded_dict, 'rb') as read_loaded_dict:
        global new_dict
        new_dict = p.load(read_loaded_dict)
    global main_sessions
    # /* Variables needed for calculating today and the diffrence of time

    time_1 = eval(input("Enter work start time : "))
    start_time = datetime(_year,_month,_today,time_1)
    end_time = datetime(_year,_month,_today, datetime.today().hour)
    time_left = end_time - start_time

    # */
    counter = 1
    main_sessions = {} # Storing all the data of the tasks here. can be saved by
    # via pickle for saving loading tasks ! (testing needed)
    # ---------------
    for item in new_dict: # displaying the work starts here
        print("Task: \"{}\"".format(item), end=' ')
        # /* Show work time duration string
        if time_multplier == 1: # First time display only
            print((start_time).time(),'-', (start_time+timedelta(
            minutes = work_time*time_multplier)).time()
            )
        else:
            display_time_left = timedelta(minutes = work_time*(time_multplier-1)
            + rest_time*(time_multplier-1)
            )
            display_time_right = timedelta(minutes = work_time*time_multplier
            + (rest_time*(time_multplier-1))
            )
            print((start_time + display_time_left).time(),'-',
            (start_time+display_time_right).time())
         # */ End of work time duration string
         # /* Show rest time duration string
        if rest_multplier <= tasks_before_15_minutes_rest:
            print("Take a rest for {} minutes".format(rest_time))
        else:
            rest_multplier = 1
            start_time = start_time + timedelta(minutes = 10)
            # for some reaosn I need to write 10 to add 15 mins
            print("Take a rest for {} minutes".format(big_rest_time))
        time_multplier += 1
        rest_multplier += 1

if __name__ == "__main__":
    pass
