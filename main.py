from datetime import timedelta, datetime
import pickle as p
import os

dir_of_tsk = "./lists"
os.system('clear')

def save_list(to_be_saved):  # The holy editing pickle mechanisim
    save_name = input("Write the name of the list: ")
    save_name = dir_of_tsk + '/' + save_name + '.tsk'
    try:
        with open(save_name, 'rb') as r2w:
            oldDict = p.load(r2w)
            oldDict.update(main_sessions)
        with open(save_name, 'wb') as w:  # pickles the 'new' dict
            p.dump(oldDict, w)
    except FileNotFoundError:  # if the file does not exist
        try:
            with open(save_name, 'wb') as w:
                p.dump(to_be_saved, w)
                return
        except FileNotFoundError:
            os.mkdir(dir_of_tsk)
            with open(save_name, 'wb') as w:
                p.dump(to_be_saved, w)

def update_list(file, list):
    file = "{}/{}".format(dir_of_tsk, file)
    try:
        with open(file, 'rb') as r2w:
            to_be_updated = p.load(r2w)
            to_be_updated.update(main_sessions)
        with open(file, 'wb') as w:  # pickles the 'new' dict
            p.dump(to_be_updated, w)
    except FileNotFoundError:  # if the file does not exist
        with open(file, 'wb') as w:
            p.dump(list, w)

def replace_list(file, list):
    file = "{}/{}".format(dir_of_tsk, file)
    try:
        with open(file, 'wb') as w:  # pickles the 'new' dict
            p.dump(list, w)
    except FileNotFoundError:  # if the file does not exist
        try:
            with open(file, 'wb') as w:
                p.dump(list, w)
        except FileNotFoundError:
            os.mkdir(dir_of_tsk)
            with open(file, 'wb') as w:
                p.dump(list, w)

def sessions():
    from global_config import (zero_time, _today, _month, _year, work_time,
                               rest_time, big_rest_time, rest_multplier,
                               tasks_before_15_minutes_rest, time_multplier)
    global main_sessions
    # /* Variables needed for calculating today and the diffrence of time
    time_1 = eval(input("Enter work start time : "))
    time_2 = eval(input("Enter work end time : "))
    print('')
    start_time = datetime(_year, _month, _today, time_1)
    end_time = datetime(_year, _month, _today, time_2)
    time_left = end_time - start_time
    # */
    counter = 1
    main_sessions = {}  # Storing all the data of the tasks here. can be saved by
    # via pickle for saving loading tasks ! (testing needed)

    # ---------------
    do_able_counter = 0
    # Used to calculate how many tasks are do(able) in the given time period
    while time_left >= zero_time:  # Loop to calculate how many tasks are doable.
        counter += 1
        time_left = time_left - timedelta(minutes=work_time + rest_time)
        do_able_counter += 1
    # Print is used to display information
    print("""Work session is {} minutes,
Rest time is {} minutes,
Number of possible tasks is {}.\n""".format(work_time, rest_time, do_able_counter))
    time_left = end_time - start_time
    counter = 1
    # Main loop start
    while time_left >= zero_time:
        # Variable defining and prompting for task names
        name_of_task_to_add = input("Name of task {}: ".format(counter))
        txt = name_of_task_to_add
        main_sessions[txt] = work_time
        counter += 1
        time_left = time_left - timedelta(minutes=work_time + rest_time)
    os.system('clear')
    for item in main_sessions:  # displaying the work starts here
        print("Do \"{}\" for {} minutes".format(item, main_sessions[item]),\
              end=' ')
        if time_multplier == 1:  # First time display only
            print(start_time.time(), '-', (start_time + timedelta(
                minutes=work_time * time_multplier)).time()
                  )
        else:
            display_time_left = timedelta(
                minutes=work_time * (time_multplier - 1)
                        + rest_time * (time_multplier - 1)
            )
            display_time_right = timedelta(
                minutes=work_time * time_multplier
                        + (rest_time * (time_multplier - 1))
            )
            print((start_time + display_time_left).time(), '-', \
                  (start_time + display_time_right).time())

        if rest_multplier <= tasks_before_15_minutes_rest:
            print("Take a rest for {} minutes".format(rest_time))
        else:
            rest_multplier = 1
            start_time = start_time + timedelta(minutes=10)
            # for some reaosn I need to write 10 to add 15 mins
            print("Take a rest for {} minutes".format(big_rest_time))
        time_multplier += 1
        rest_multplier += 1
    print('='*20)
    print("Options:(S)ave new list\n \t(A)dd to a saved list\n \t(R)eplace saved list\n \
\t(D)on't save")
    save_choice = input()
    if save_choice.lower() == 's':  # Save dict
        save_list(main_sessions)
    elif save_choice.lower() == 'a':  # Update dict
        list_of_items_to_update = [i for i in os.listdir(str(dir_of_tsk))
                                   if i.endswith(".tsk")]
        for item in list_of_items_to_update:
            print("{}- {}".format((list_of_items_to_update.index(item)) + 1, item))
        choose = eval(input("Choose what list to update: "))
        choose -= 1
        update_list(list_of_items_to_update[choose], main_sessions)

    elif save_choice.lower() == 'r':  # Update dict
        list_of_items_to_replace = [i for i in os.listdir(str(dir_of_tsk))
                                   if i.endswith(".tsk")]
        for item in list_of_items_to_replace:
            print("{}- {}".format((list_of_items_to_replace.index(item)) + 1, item))
        choose = eval(input("Choose what list to update: "))
        choose -= 1
        replace_list(list_of_items_to_replace[choose],main_sessions)


    elif save_choice.lower() == 'd':
        # Doesn't save, can be used for a quick one-time list
        pass


if __name__ == "__main__":
    from load_task import load

    print("Welcome to Tasker v 1.4 beta\nWhat do you want to do ?\n- (N)ew list\n\
- (L)oad list\n- (D)elete list\n- (E)xit")
    while True:
        choice = input()
        if choice.lower() == 'n':
            sessions()
            break
        elif choice.lower() == 'l':
            try:
                list_of_items = [i for i in os.listdir(dir_of_tsk) \
                                 if i.endswith(".tsk")]
                for item in list_of_items:
                    print("{}- {}".format((list_of_items.index(item)) + 1, item))
                choose = eval(input("Choose what list to load: "))
                choose -= 1
                load(list_of_items[choose])
                break
            except FileNotFoundError:
                print("You don't have any saved lists\nPlease select N for a\
new list")

        elif choice.lower() == 'd':
            try:
                list_of_items = [i for i in os.listdir(dir_of_tsk) \
                                 if i.endswith(".tsk")]
                for item in list_of_items:
                    print("{}- {}".format((list_of_items.index(item)) + 1, item))
                choose = eval(input("Choose what list to delete: "))
                choose -= 1
                os.remove(dir_of_tsk+'/'+list_of_items[choose])
                break
            except FileNotFoundError:
                print("You don't have any saved lists\nPlease select N for a\
new list")

        elif choice.lower() == 'e':
            exit()
        else:
            print('Please choose a vaild option')
