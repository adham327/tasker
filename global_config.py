from datetime import timedelta, datetime

# Change the default values from here
work_time = 25 # Work shift.
rest_time = 5 # Rest shift.
big_rest_time = 15 # Big rest time.
tasks_before_15_minutes_rest = 2


while True:
    want_to_change = input("Do you want to change the default values ? (y) (n)\n")
    if want_to_change.lower() == 'y':

        print("Enter values in minutes")

        inp = eval(input("Work time (default = {}): ".format(work_time)))
        work_time = inp

        inp = eval(input("Rest time (default = {}): ".format(rest_time)))
        rest_time = inp

        inp = eval(input("Big rest time (default = {}): ".format(big_rest_time)))
        big_rest_time = inp

        inp = eval(input("Number of tasks before 15 minutes rest (default = {}): \
    ".format(tasks_before_15_minutes_rest)))
        tasks_before_15_minutes_rest = inp
        break
    elif want_to_change.lower() == 'n':
        pass
        break
    else:
        print("Please enter a valid choice !")



zero_time = timedelta(minutes = 0, hours = 0, seconds = 0)
_today = datetime.today().day
_month = datetime.today().month
_year = datetime.today().year
time_multplier = 1 # Used when displaying the hours of work.
rest_multplier = 1 # Used when displaying the time of rest.
from os import system
system('clear')
